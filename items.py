class Weapon:
    def __init__(self):
        raise NotImplementedError("Do not create raw Weapon objects.")

    def __str__(self):
        return self.name


class Rock(Weapon):
    def __init__(self):
        self.name = "Rock"
        self.description = "A fist-sized rock, suitable for smack'n heads and stuff"
        self.damage = 2


class Dagger(Weapon):
    def __init__(self):
        self.name = "Dagger"
        self.description = "Stabby knife. " \
                           "More dangerous than smacky rock"
        self.damage = 10


class RustySword(Weapon):
    def __init__(self):
        self.name = "Rusty Sword"
        self.description = "A longer variant of the stabby knife. " \
                           "Lightly rusted for that sweet, rustic flavor"
        self.damage = 20


class CrackedShield(Weapon):
    def __init__(self):
        self.name = "Cracked Shield"
        self.description = "An old, damaged shield. " \
                           "Probably from a long 'n stabby attack."
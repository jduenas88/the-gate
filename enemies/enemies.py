from enemies import enemy_types


class Enemy:
    def __init__(self):
        raise NotImplementedError("do not create raw enemy objects!")

    def __str__(self):
        return self.name

    def is_alive(self):
        return self.hp > 0


class GiantSpider(Enemy):
    def __init__(self):
        self.name = "Giant Spider"
        self.hp = 10
        self.damage = 2
        self.enemy_type = enemy_types.Bugtoid()


class Ogre(Enemy):
    def __init__(self):
        self.name = "Ogre"
        self.hp = 30
        self.damage = 10
        self.enemy_type = enemy_types.Humanoid()


class BatColony(Enemy):
    def __init__(self):
        self.name = "Colony of bats"
        self.hp = 100
        self.damage = 4
        self.enemy_type = enemy_types.Bestial()


class RockMonster(Enemy):
    def __init__(self):
        self.name = "Rock Monster"
        self.hp = 80
        self.damage = 15
        self.enemy_type = enemy_types.Elemental()


class RoguePirate(Enemy):
    def __init__(self):
        self.name = "Rogue Pirate"
        self.hp = 20
        self.damage = 5
        self.enemy_type = enemy_types.Humanoid()
class EnemyType:
    def __init__(self):
        raise NotImplementedError("do not create raw EnemyType objects!")

    def __str__(self):
        return self.name


class Humanoid(EnemyType):
    def __init__(self):
        self.name = "Humanoid"
        self.description = "Human-like creatures. Mostly hairless, bipedal, and typical."
        self.weak_to = "melee physical"
        self.resistant_to = "elemental"


class Dragonoid(EnemyType):
    def __init__(self):
        self.name = "Dragonoid"
        self.description = "Dragon-like creatures. Typically scaley skinned, magical, and angry."
        self.weak_to = "elemental"
        self.resistant_to = "melee physical"


class Bugtoid(EnemyType):
    def __init__(self):
        self.name = "Bugtoid"
        self.description = "Like the ones on the ground, but bigger! Usually has at least 6-8 legs."
        self.weak_to = "magic"
        self.resistant_to = "ranged physical"


class Elemental(EnemyType):
    def __init__(self):
        self.name = "Elemental"
        self.description = "Beings consisting mostly of the primal elements."
        self.weak_to = "magic"
        self.resistant_to = "elemental"


class Bestial(EnemyType):
    def __init__(self):
        self.name = "Bestial"
        self.description = "Usually furry, quadrupedal, and relatively small...but not always."
        self.weak_to = "melee physical"
        self.resistant_to = "ranged physical"

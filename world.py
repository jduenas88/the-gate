import random
from enemies import enemies


class MapTile:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def intro_text(self):
        raise NotImplementedError("Create a subclass instead!")

    def modify_player(self, player):
        pass


class StartTile(MapTile):
    def intro_text(self):
        return """
        You awake from a deep slumber feeling very groggy. You vaguely recall a dream about a window--or was it a door?
        A hatch, perhaps? The memory quickly fades and you decide to assess your surroundings.
        
        From your bed you can see a small wooden door on the far side of the room, a dresser with various items on it, 
        and a small window you're not sure you could fit through even if you were so inclined. 
        """


class BoringTile(MapTile):
    def intro_text(self):
        return """
        Hmm...not much to see in here...
        """


class Hallway(MapTile):
    def intro_text(self):
        return """
        Just a boring old hallway.
        """


class VictoryTile(MapTile):
    def intro_text(self):
        return """
        I should figure out a proper ending, but hey man you reached the gate!
        """


class EnemyTile(MapTile):
    def __init__(self, x, y):
        r = random.random()
        if r < 0.50:
            self.enemy = enemies.GiantSpider()
        elif r < 0.80:
            self.enemy = enemies.Ogre()
        elif r < 0.95:
            self.enemy = enemies.BatColony()
        else:
            self.enemy = enemies.RockMonster()

        super().__init__(x, y)

    def intro_text(self):
        if self.enemy.is_alive():
            return "A {} awaits! Looks to be a {} type creature!".format(self.enemy.name, self.enemy.enemy_type.name)
        else:
            return "You've defeated the {}!".format(self.enemy.name)

    def modify_player(self, player):
        if self.enemy.is_alive():
            player.hp = player.hp - self.enemy.damage
            print("Enemy does {} damage. You have {} HP remaining".format(self.enemy.damage, player.hp))


world_map = [
    [None, VictoryTile(1, 0), None],
    [None, BoringTile(1, 1), None],
    [EnemyTile(0, 2), StartTile(1, 2), BoringTile(2, 2)],
    [None, BoringTile(1, 3), None]
]


def tile_at(x, y):
    if x < 0 or y < 0:
        return None
    try:
        return world_map[y][x]
    except IndexError:
        return None


# def render_map():
#     i = 1
#     print("+---------+")
#     output = ""
#     for tile_list in world_map:
#         for tile in tile_list:
#             if tile is not None:
#                 if tile.classification == "room":
#                     output += "[_]"
#                 elif tile.classification == "hall":
#                     output += "==="
#                 elif tile.classification == "start":
#                     output += "[s]"
#             else:
#                 output += "|"
#             if i < 3:
#                 pass
#                 i = i + 1
#             else:
#                 print("|" + output + "|")
#                 output = ""
#                 i = 1
#
#     print("+---------+")



from combat import weapons


class Container:
    def __init__(self):
        self.items = []
        raise NotImplementedError("do not create raw enemy objects!")

    def __str__(self):
        return self.name

    def is_empty(self):
        if len(self.items) > 0:
            return False
        else:
            return True

    def add_to_inventory(self, item):
        self.items.append(item)


class Inventory(Container):
    def __init__(self):
        self.name = "Inventory"
        self.items = []
        self.generate_starter_items()

    def __str__(self):
        return self.items

    def generate_starter_items(self):
        starter_items = {
            weapons.TrainingSword(),
            weapons.ElementalRuneRock()
        }

        for item in starter_items:
            self.add_to_inventory(item)




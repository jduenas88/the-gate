from combat import damage_types


class Weapon:
    def __init__(self):
        raise NotImplementedError("do not create raw Weapon objects!")

    def __str__(self):
        return self.name


class TrainingSword(Weapon):
    def __init__(self):
        self.name = "Training Sword"
        self.description = "A basic melee weapon. I think the handle is metal and the blade wood?!"
        self.damage_type = damage_types.MeleePhysical()
        self.min_damage = 2
        self.max_damage = 5


class RustyDagger(Weapon):
    def __init__(self):
        self.name = "Rusty Dagger"
        self.description = "A basic melee weapon. Possibly has a tetanus DoT (it doesn't)."
        self.damage_type = damage_types.MeleePhysical()
        self.min_damage = 1
        self.max_damage = 6


class FrayedBow(Weapon):
    def __init__(self):
        self.name = "Frayed Bow"
        self.description = "A basic ranged weapon. Looks like someone mistook it for and used it as a melee weapon."
        self.damage_type = damage_types.RangedPhysical()
        self.min_damage = 0
        self.max_damage = 10


class ThrowableClayPot(Weapon):
    def __init__(self):
        self.name = "Throwable Clay Pot"
        self.description = "A very nice, hand-crafted clay pot. Very different from a Smashable Clay Pot."
        self.damage_type = damage_types.RangedPhysical()
        self.min_damage = 1
        self.max_damage = 5


class WoodenStaff(Weapon):
    def __init__(self):
        self.name = "Wooden Staff"
        self.description = "Power emanates from this skinny tree branch."
        self.damage_type = damage_types.Magical()
        self.min_damage = 0
        self.max_damage = 10


class ElementalRuneRock(Weapon):
    def __init__(self):
        self.name = "Elemental Rune Rock"
        self.description = "A regular old rock, except this one has a glowing rune on it."
        self.damage_type = damage_types.Elemental()
        self.min_damage = 3
        self.max_damage = 6
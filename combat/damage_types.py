class DamageType:
    def __init__(self):
        raise NotImplementedError("do not create raw DamageType objects!")

    def __str__(self):
        return self.name


class MeleePhysical(DamageType):
    def __init__(self):
        self.name = "Melee Physical"
        self.description = "Sword strikes, knife stabs, hulk smashes."


class RangedPhysical(DamageType):
    def __init__(self):
        self.name = "Ranged Physical"
        self.description = "Arrow shows, bullet shots, vases thrown across the room."


class Magical(DamageType):
    def __init__(self):
        self.name = "Magic"
        self.description = "Arcane blasts, necromancy, waves of the Kamehameha."


class Elemental(DamageType):
    def __init__(self):
        self.name = "Elemental"
        self.description = "Fireballs, conjured water, rocky hands summoned from the earth"

from player import Player
import world
from containers import containers
from colorama import Fore, Back, Style, init


def play():
    print("The Gate")
    player = Player()
    while True:
        room = world.tile_at(player.x, player.y)
        print(room.intro_text())

        print(Fore.GREEN + "[n=North, e=East, s=South, w=West, i=Inventory]" + Style.RESET_ALL)


        room.modify_player(player)
        action_input = get_player_command()
        if action_input in ['n', 'N']:
            player.move_north()
        elif action_input in ['s', 'S']:
            player.move_south()
        elif action_input in ['e', 'E']:
            player.move_east()
        elif action_input in ['w', 'W']:
            player.move_west()
        elif action_input in ['i', 'I', 'b', 'B']:
            player.print_inventory()
        elif action_input in ['bw']:
            print(player.most_powerful_weapon())
        elif action_input in ['a', 'A', 'attack']:
            player.attack()
        elif action_input in ['test']:
            inventory = containers.Inventory().items
            for item in inventory:
                print(item)
        else:
            print("Invalid action!")


def get_player_command():
    return input('Action: ')

play()
